<!-- Begin Bottom -->
	<section class="bottom" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<ul class="accordion" data-accordion data-allow-all-closed="true">
					<li class="accordion-item" data-accordion-item>
						<a href="#" class="accordion-title">Nuestras Políticas</a>
						<div class="accordion-content" data-tab-content>
							<?php get_template_part( 'part', 'menu-bottom' ); ?>
						</div>
					</li>
				</ul>
				<?php dynamic_sidebar( 'bottom' ); ?>
			</div>
		</div>
	</section>
<!-- End Bottom -->