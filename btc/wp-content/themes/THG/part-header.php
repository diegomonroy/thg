<!-- Begin Top -->
	<section class="top" data-wow-delay="0.5s">
		<div class="row collapse align-justify align-middle">
			<div class="small-6 medium-3 columns">
				<?php dynamic_sidebar( 'logo' ); ?>
			</div>
			<div class="small-6 medium-2 columns">
				<?php dynamic_sidebar( 'logo_reed_and_mackay' ); ?>
			</div>
		</div>
		<div class="row collapse align-center align-middle">
			<div class="small-12 columns">
				<?php get_template_part( 'part', 'menu' ); ?>
			</div>
		</div>
	</section>
<!-- End Top -->