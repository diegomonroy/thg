<!-- Begin Banner -->
	<section class="banner" data-wow-delay="0.5s">
		<div class="row collapse">
			<div class="small-12 columns">
				<?php if ( is_front_page() ) : dynamic_sidebar( 'banner_inicio' ); endif; ?>
				<?php if ( is_page( array( 'sobre-nosotros' ) ) ) : dynamic_sidebar( 'banner_sobre_nosotros' ); endif; ?>
				<?php if ( is_page( array( 'nuestro-equipo' ) ) ) : dynamic_sidebar( 'banner_nuestro_equipo' ); endif; ?>
				<?php if ( is_page( array( 'servicios' ) ) ) : dynamic_sidebar( 'banner_servicios' ); endif; ?>
				<?php if ( is_page( array( 'corporate-travel-management' ) ) ) : dynamic_sidebar( 'banner_corporate_travel_management' ); endif; ?>
				<?php if ( is_page( array( 'eventos-y-congresos' ) ) ) : dynamic_sidebar( 'banner_eventos_y_congresos' ); endif; ?>
				<?php if ( is_page( array( 'soluciones-y-tecnologia' ) ) ) : dynamic_sidebar( 'banner_soluciones_y_tecnologia' ); endif; ?>
				<?php if ( is_page( array( 'nuestra-tecnologia' ) ) ) : dynamic_sidebar( 'banner_nuestra_tecnologia' ); endif; ?>
				<?php if ( is_page( array( 'contacto' ) ) ) : dynamic_sidebar( 'banner_contacto' ); endif; ?>
				<?php if ( is_page( array( 'smmp-model' ) ) ) : dynamic_sidebar( 'banner_smmp_model' ); endif; ?>
				<?php if ( is_page( array( 'health-care-division' ) ) ) : dynamic_sidebar( 'banner_health_care_division' ); endif; ?>
				<?php if ( is_page( array( 'cvent-tools' ) ) ) : dynamic_sidebar( 'banner_cvent_tools' ); endif; ?>
			</div>
		</div>
	</section>
<!-- End Banner -->