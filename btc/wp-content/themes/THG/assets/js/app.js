// JavaScript Document

/* ************************************************************************************************************************

BTC

File:			app.js
Author:			Amapolazul
Homepage:		www.amapolazul.com
Copyright:		2017

************************************************************************************************************************ */

/* Foundation */

$(document).foundation();

/* WOW */

new WOW().init();

/* jQuery */

jQuery.noConflict();

jQuery(document).ready(function () {
	/* Menu */
	jQuery( 'ul.sub-menu' ).addClass( 'menu' );
	var nav = jQuery( '.top-bar-left' );
	jQuery( window ).scroll(function () {
		if ( jQuery( this ).scrollTop() > 160 ) {
			nav.addClass( 'f-nav' );
		} else {
			nav.removeClass( 'f-nav' );
		}
	});
	/* Newsletter */
	jQuery( '.tnp-email' ).attr( 'placeholder', 'insertar correo aquí' );
	jQuery( 'input.tnp-submit' ).prop( 'value', 'ENVIAR' );
});

/* Back To Top */

if ( jQuery( '#back-to-top' ).length ) {
	var scrollTrigger = 100,
	backToTop = function () {
		var scrollTop = jQuery( window ).scrollTop();
		if ( scrollTop > scrollTrigger ) {
			jQuery( '#back-to-top' ).addClass( 'show' );
		} else {
			jQuery( '#back-to-top' ).removeClass( 'show' );
		}
	};
	backToTop();
	jQuery( window ).on( 'scroll', function () {
		backToTop();
	});
	jQuery( '#back-to-top' ).on( 'click', function ( e ) {
		e.preventDefault();
		jQuery( 'html, body' ).animate({
			scrollTop: 0
		}, 700);
	});
}