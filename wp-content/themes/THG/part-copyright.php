<?php
$lang = get_locale();
switch ( $lang ) {
	default:
	case 'es_ES':
		$text_1 = 'Todos los derechos reservados';
		$text_2 = 'Sitio desarrollado por';
		break;
	case 'en_US':
		$text_1 = 'All rights reserved';
		$text_2 = 'Site developed by';
		break;
}
?>
<!-- Begin Copyright -->
	<div class="copyright text-center" data-wow-delay="0.5s">
		&copy; <?php echo date( 'Y' ); ?> <a href="<?php echo site_url(); ?>"><?php bloginfo(name); ?></a>. <?php echo $text_1; ?>. <?php echo $text_2; ?> <a href="http://www.amapolazul.com" target="_blank">Amapolazul</a>.
	</div>
<!-- End Copyright -->