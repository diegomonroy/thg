<!-- Begin Content Image -->
	<section class="content_image" data-wow-delay="0.5s">
		<div class="row expanded collapse">
			<div class="small-12 columns text-center">
				<?php if ( is_page( array( 'corporate-travel' ) ) ) : dynamic_sidebar( 'content_image_corporate_travel' ); endif; ?>
				<?php if ( is_page( array( 'reportes-kpi-de-servicio', 'smart-account-management' ) ) ) : dynamic_sidebar( 'content_image_reportes_kpi_de_servicio' ); endif; ?>
				<?php if ( is_page( array( 'servicios-para-asociaciones-medicas', 'pharma-healthcare-division' ) ) ) : dynamic_sidebar( 'content_image_eventos_asociacion_cientificas' ); endif; ?>
				<?php if ( is_page( array( 'viajes-personales' ) ) ) : dynamic_sidebar( 'content_image_viajes_personales' ); endif; ?>
			</div>
		</div>
	</section>
<!-- End Content Image -->