<?php

/*

Functions for my template

*/

/*
 * Function to add my styles files
 */
function my_styles_files() {
	wp_enqueue_style( 'foundation-css', get_template_directory_uri() . '/build/foundation/css/foundation.min.css', false );
	wp_enqueue_style( 'animate-css', get_template_directory_uri() . '/build/bower_components/animate.css/animate.min.css', false );
	wp_enqueue_style( 'fancybox-css', get_template_directory_uri() . '/build/bower_components/fancybox/dist/jquery.fancybox.min.css', false );
	wp_enqueue_style( 'font-awesome-css', get_template_directory_uri() . '/build/bower_components/font-awesome/css/font-awesome.min.css', false );
	if ( is_child_theme() ) {
		wp_enqueue_style( 'parent-css', trailingslashit( get_template_directory_uri() ) . 'style.css', false );
	}
	wp_enqueue_style( 'theme-css', get_stylesheet_uri(), false );
}
add_action( 'wp_enqueue_scripts', 'my_styles_files' );

/*
 * Function to add my scripts files
 */
function my_scripts_files() {
	//wp_deregister_script( 'jquery' );
}
add_action( 'wp_enqueue_scripts', 'my_scripts_files' );

/*
 * Function to add my scripts files in footer
 */
function my_scripts_files_footer() {
	//wp_enqueue_script( 'jquery-js', get_template_directory_uri() . '/build/bower_components/jquery/dist/jquery.min.js', false );
	wp_enqueue_script( 'what-input-js', get_template_directory_uri() . '/build/bower_components/what-input/dist/what-input.min.js', false );
	wp_enqueue_script( 'foundation-js', get_template_directory_uri() . '/build/foundation/js/vendor/foundation.min.js', false );
	wp_enqueue_script( 'fancybox-js', get_template_directory_uri() . '/build/bower_components/fancybox/dist/jquery.fancybox.min.js', false );
	//wp_enqueue_script( 'queryloader2-js', get_template_directory_uri() . '/build/bower_components/QueryLoader2/queryloader2.min.js', false );
	wp_enqueue_script( 'wow-js', get_template_directory_uri() . '/build/bower_components/wow/dist/wow.min.js', false );
	wp_enqueue_script( 'theme-js', get_template_directory_uri() . '/build/app.js', false );
}
add_action( 'wp_footer', 'my_scripts_files_footer' );

/*
 * Function to register my menus
 */
function register_my_menus() {
	register_nav_menus(
		array(
			'bottom-menu' => __( 'Bottom Menu' ),
			'main-menu' => __( 'Main Menu' )
		)
	);
}
add_action( 'init', 'register_my_menus' );

/*
 * Function to register my sidebars and widgetized areas
 */
function arphabet_widgets_init() {
	register_sidebar(
		array(
			'name' => 'Logo',
			'id' => 'logo',
			'before_widget' => '<div class="moduletable_to1 text-center">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Logo ATG Colombia',
			'id' => 'logo_atg_colombia',
			'before_widget' => '<div class="moduletable_to4 text-center">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Idioma',
			'id' => 'language',
			'before_widget' => '<div class="moduletable_to5 text-center">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Phone',
			'id' => 'phone',
			'before_widget' => '<div class="moduletable_to2">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Inicio',
			'id' => 'banner_inicio',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Quiénes Somos',
			'id' => 'banner_quienes_somos',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Antes de THG',
			'id' => 'banner_antes_de_thg',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Qué es Diferente?',
			'id' => 'banner_que_es_diferente',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Consultoría de Viajes',
			'id' => 'banner_consultoria_de_viajes',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner SLA, Reportes & KPI',
			'id' => 'banner_sla_reportes_kpi',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Equipos Dedicados',
			'id' => 'banner_equipos_dedicados',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Corporate Travel',
			'id' => 'banner_corporate_travel',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner The VIP Experience',
			'id' => 'banner_the_vip_experience',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Eventos Perfectos',
			'id' => 'banner_eventos_perfectos',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner SMMP Model',
			'id' => 'banner_smmp_model',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Health Care Division',
			'id' => 'banner_health_care_division',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner CVENT Tools',
			'id' => 'banner_cvent_tools',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Selección de Sedes (VENUE)',
			'id' => 'banner_seleccion_de_sedes_venue',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Procurement y Contratos',
			'id' => 'banner_procurement_y_contratos',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Modelos de Servicio',
			'id' => 'banner_modelos_de_servicio',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Plataformas de Autogestión',
			'id' => 'banner_plataformas_de_autogestion',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Content Corporate Travel',
			'id' => 'content_image_corporate_travel',
			'before_widget' => '<div class="moduletable_ci1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Content Reportes & KPI de Servicio',
			'id' => 'content_image_reportes_kpi_de_servicio',
			'before_widget' => '<div class="moduletable_ci1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Content Image Eventos Asociación Científicas',
			'id' => 'content_image_eventos_asociacion_cientificas',
			'before_widget' => '<div class="moduletable_ci1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Content Image Viajes Personales',
			'id' => 'content_image_viajes_personales',
			'before_widget' => '<div class="moduletable_ci1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 1',
			'id' => 'block_1',
			'before_widget' => '<div class="moduletable_b11">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 2',
			'id' => 'block_2',
			'before_widget' => '<div class="moduletable_b21">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 3',
			'id' => 'block_3',
			'before_widget' => '<div class="moduletable_b31">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 4',
			'id' => 'block_4',
			'before_widget' => '<div class="moduletable_b41">',
			'after_widget' => '</div>',
			'before_title' => '<h2>',
			'after_title' => '</h2>'
		)
	);
	register_sidebar(
		array(
			'name' => 'Bottom',
			'id' => 'bottom',
			'before_widget' => '<div class="moduletable_bo2">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
}
add_action( 'widgets_init', 'arphabet_widgets_init' );