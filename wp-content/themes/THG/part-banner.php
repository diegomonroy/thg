<!-- Begin Banner -->
	<section class="banner" data-wow-delay="0.5s">
		<div class="row expanded collapse">
			<div class="small-12 columns">
				<!-- Inicio -->
				<?php if ( is_front_page() ) : dynamic_sidebar( 'banner_inicio' ); endif; ?>
				<!-- Nosotros -->
				<?php if ( is_page( array( 'quienes-somos', 'who-we-are' ) ) ) : dynamic_sidebar( 'banner_quienes_somos' ); endif; ?>
				<?php if ( is_page( array( 'antes-de-thg' ) ) ) : dynamic_sidebar( 'banner_antes_de_thg' ); endif; ?>
				<?php if ( is_page( array( 'que-es-diferente', 'what-is-different' ) ) ) : dynamic_sidebar( 'banner_que_es_diferente' ); endif; ?>
				<!-- Servicios -->
				<!-- Agencia de Viajes Corporativos -->
				<?php if ( is_page( array( 'corporate-travel' ) ) ) : dynamic_sidebar( 'banner_corporate_travel' ); endif; ?>
				<?php if ( is_page( array( 'modelos-de-servicio', 'service-configurations' ) ) ) : dynamic_sidebar( 'banner_modelos_de_servicio' ); endif; ?>
				<?php if ( is_page( array( 'plataformas-de-autogestion', 'self-booking-platforms' ) ) ) : dynamic_sidebar( 'banner_plataformas_de_autogestion' ); endif; ?>
				<?php if ( is_page( array( 'reportes-kpi-de-servicio', 'smart-account-management' ) ) ) : dynamic_sidebar( 'banner_sla_reportes_kpi' ); endif; ?>
				<?php if ( is_page( array( 'tecnologia-diferencial', 'differential-technology' ) ) ) : dynamic_sidebar( 'banner_consultoria_de_viajes' ); endif; ?>
				<!-- Agencia de Congresos y Eventos -->
				<?php if ( is_page( array( 'organizacion-de-eventos', 'meetings-events-agency' ) ) ) : dynamic_sidebar( 'banner_eventos_perfectos' ); endif; ?>
				<?php if ( is_page( array( 'seleccion-de-sedes-venue' ) ) ) : dynamic_sidebar( 'banner_seleccion_de_sedes_venue' ); endif; ?>
				<?php if ( is_page( array( 'contratos-y-procurement', 'contracts-preferred-vendors' ) ) ) : dynamic_sidebar( 'banner_procurement_y_contratos' ); endif; ?>
				<?php if ( is_page( array( 'servicios-para-asociaciones-medicas', 'pharma-healthcare-division' ) ) ) : dynamic_sidebar( 'banner_health_care_division' ); endif; ?>
				<?php if ( is_page( array( 'smmp-global-programs' ) ) ) : dynamic_sidebar( 'banner_smmp_model' ); endif; ?>
				<?php if ( is_page( array( 'tecnologia-dedicada' ) ) ) : dynamic_sidebar( 'banner_cvent_tools' ); endif; ?>
				<!-- Agencia de BTL y Producción -->
				<!-- Viajes Personales -->
				<?php if ( is_page( array( 'viajes-personales' ) ) ) : dynamic_sidebar( 'banner_the_vip_experience' ); endif; ?>
			</div>
		</div>
	</section>
<!-- End Banner -->