// JavaScript Document

/* ************************************************************************************************************************

THG

File:			app.js
Author:			Amapolazul
Homepage:		www.amapolazul.com
Copyright:		2017

************************************************************************************************************************ */

/* Foundation */

$(document).foundation();

/* WOW */

new WOW().init();

/* jQuery */

jQuery.noConflict();

jQuery(document).ready(function () {
	/* Load */
	/*jQuery( 'body' ).queryLoader2({
		backgroundColor: '#fff',
		barColor: '#2c334e',
		barHeight: 0,
		deepSearch: true,
		percentage: true,
		onComplete: function() {
			new WOW().init();
		}
	});*/
	/* Menu */
	jQuery( 'ul.sub-menu' ).addClass( 'menu' );
	/* Newsletter */
	var lang = jQuery( '#lang').text();
	switch ( lang ) {
		case 'es_ES':
			jQuery( '.tnp-email' ).attr( 'placeholder', 'insertar correo aquí' );
			jQuery( 'input.tnp-submit' ).prop( 'value', 'ENVIAR' );
			break;
		case 'en_US':
			jQuery( '.tnp-email' ).attr( 'placeholder', 'insert mail here' );
			jQuery( 'input.tnp-submit' ).prop( 'value', 'SUBMIT' );
			break;
	}
});

/* Back To Top */

if ( jQuery( '#back-to-top' ).length ) {
	var scrollTrigger = 100,
	backToTop = function () {
		var scrollTop = jQuery( window ).scrollTop();
		if ( scrollTop > scrollTrigger ) {
			jQuery( '#back-to-top' ).addClass( 'show' );
		} else {
			jQuery( '#back-to-top' ).removeClass( 'show' );
		}
	};
	backToTop();
	jQuery( window ).on( 'scroll', function () {
		backToTop();
	});
	jQuery( '#back-to-top' ).on( 'click', function ( e ) {
		e.preventDefault();
		jQuery( 'html, body' ).animate({
			scrollTop: 0
		}, 700);
	});
}