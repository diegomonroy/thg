<!-- Begin Top -->
	<section class="top" data-wow-delay="0.5s">
		<div class="row collapse align-center align-middle">
			<div class="small-1 columns hide-for-medium"></div>
			<div class="small-4 medium-2 columns">
				<?php dynamic_sidebar( 'logo' ); ?>
			</div>
			<div class="small-1 columns hide-for-medium"></div>
			<div class="small-5 columns hide-for-medium">
				<?php dynamic_sidebar( 'logo_atg_colombia' ); ?>
			</div>
			<div class="small-1 columns hide-for-medium"></div>
			<div class="small-12 medium-8 columns">
				<?php dynamic_sidebar( 'phone' ); ?>
				<?php dynamic_sidebar( 'language' ); ?>
				<div class="clear"></div>
				<?php get_template_part( 'part', 'menu' ); ?>
			</div>
			<div class="medium-2 columns hide-for-small-only">
				<?php dynamic_sidebar( 'logo_atg_colombia' ); ?>
			</div>
		</div>
	</section>
<!-- End Top -->