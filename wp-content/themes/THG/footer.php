		<?php get_template_part( 'part', 'content-image' ); ?>
		<?php if ( is_front_page() ) : get_template_part( 'part', 'block-1' ); endif; ?>
		<?php if ( ! is_page( array( 'plataformas-de-autogestion', 'conversemos', 'self-management-platforms', 'lets-talk' ) ) ) : get_template_part( 'part', 'block-2' ); endif; ?>
		<?php get_template_part( 'part', 'block-3' ); ?>
		<?php get_template_part( 'part', 'block-4' ); ?>
		<?php get_template_part( 'part', 'bottom' ); ?>
		<?php get_template_part( 'part', 'copyright' ); ?>
		<!-- Begin Back To Top -->
			<a href="#" id="back-to-top" title="Ir Arriba">Ir Arriba</a>
		<!-- End Back To Top -->
		<?php wp_footer(); ?>
	</body>
</html>