// JavaScript Document

/* ************************************************************************************************************************

Doctor Traveling

File:			app.js
Author:			Amapolazul
Homepage:		www.amapolazul.com
Copyright:		2017

************************************************************************************************************************ */

/* WOW */

new WOW().init();

/* jQuery */

jQuery.noConflict();

function onSelectShow( show, select, value ) {
	jQuery( show ).hide();
	jQuery( select ).change(function () {
		if ( jQuery( select ).val() == value ) {
			jQuery( show ).show();
		} else {
			jQuery( show ).hide();
		}
	});
}

jQuery(document).ready(function () {
	// Bootstrap Datepicker
	jQuery( '.datepicker' ).datepicker({
		format: 'dd/mm/yyyy',
		startDate: 'today',
		language: 'es'
	});
	// Bootstrap Timepicker
	jQuery( '.timepicker' ).timepicker();
	// Form
	onSelectShow( '#otra_acomodacion_field', '#tipo_de_habitacion_deseada', 'otra_acomodacion' );
	onSelectShow( '#requerimientos_especiales_field', '#tipo_de_habitacion_deseada', 'requerimientos_especiales' );
	onSelectShow( '#invitado_por_una_compania_farmaceutica_field', '#viaja', 'invitado_por_una_compania_farmaceutica' );
});

// Form

jQuery( '#submit' ).click(function ( event ) {
	event.preventDefault();
});

/*jQuery( '#form' ).submit(function ( event ) {
	return false;
});*/

function myFunction() {
	var nombre_y_apellido_del_oftalmologo = document.getElementById( 'nombre_y_apellido_del_oftalmologo' ).value;
	var ciudad_base = document.getElementById( 'ciudad_base' ).value;
	var clinica_y_o_entidad_donde_ejerce = document.getElementById( 'clinica_y_o_entidad_donde_ejerce' ).value;
	var telefonos_de_contacto = document.getElementById( 'telefonos_de_contacto' ).value;
	var email = document.getElementById( 'email' ).value;
	var prefiere_contacto_via = document.getElementById( 'prefiere_contacto_via' ).value;
	var numero_adultos = document.getElementById( 'numero_adultos' ).value;
	var numero_ninos = document.getElementById( 'numero_ninos' ).value;
	var hoteles = document.getElementById( 'hoteles' ).value;
	var tipo_de_habitacion_deseada = document.getElementById( 'tipo_de_habitacion_deseada' ).value;
	var otra_acomodacion = document.getElementById( 'otra_acomodacion' ).value;
	var requerimientos_especiales = document.getElementById( 'requerimientos_especiales' ).value;
	var fecha_de_ida = document.getElementById( 'fecha_de_ida' ).value;
	var horario_de_ida = document.getElementById( 'horario_de_ida' ).value;
	var fecha_de_regreso = document.getElementById( 'fecha_de_regreso' ).value;
	var horario_de_regreso = document.getElementById( 'horario_de_regreso' ).value;
	var numero_de_viajero_frecuente = document.getElementById( 'numero_de_viajero_frecuente' ).value;
	var tiquete_con_millas = document.getElementById( 'tiquete_con_millas' ).value;
	var otros_detalles = document.getElementById( 'otros_detalles' ).value;
	/*var actividades_sociales = { 'actividades_sociales[]' : [] };
	jQuery( 'input:checked' ).each(function () {
		actividades_sociales[ 'actividades_sociales[]' ].push( jQuery( this ).val() );
	});*/
	var actividades_sociales = jQuery( '.actividades_sociales:checked' ).serialize();
	var viaja = document.getElementById( 'viaja' ).value;
	var invitado_por_una_compania_farmaceutica = document.getElementById( 'invitado_por_una_compania_farmaceutica' ).value;
	var dataString =
		'nombre_y_apellido_del_oftalmologo1=' + nombre_y_apellido_del_oftalmologo +
		'&ciudad_base1=' + ciudad_base +
		'&clinica_y_o_entidad_donde_ejerce1=' + clinica_y_o_entidad_donde_ejerce +
		'&telefonos_de_contacto1=' + telefonos_de_contacto +
		'&email1=' + email +
		'&prefiere_contacto_via1=' + prefiere_contacto_via +
		'&numero_adultos1=' + numero_adultos +
		'&numero_ninos1=' + numero_ninos +
		'&hoteles1=' + hoteles +
		'&tipo_de_habitacion_deseada1=' + tipo_de_habitacion_deseada +
		'&otra_acomodacion1=' + otra_acomodacion +
		'&requerimientos_especiales1=' + requerimientos_especiales +
		'&fecha_de_ida1=' + fecha_de_ida +
		'&horario_de_ida1=' + horario_de_ida +
		'&fecha_de_regreso1=' + fecha_de_regreso +
		'&horario_de_regreso1=' + horario_de_regreso +
		'&numero_de_viajero_frecuente1=' + numero_de_viajero_frecuente +
		'&tiquete_con_millas1=' + tiquete_con_millas +
		'&otros_detalles1=' + otros_detalles +
		'&actividades_sociales1=' + actividades_sociales +
		'&viaja1=' + viaja +
		'&invitado_por_una_compania_farmaceutica1=' + invitado_por_una_compania_farmaceutica
	;
	console.log( dataString );
	if ( nombre_y_apellido_del_oftalmologo === '' || telefonos_de_contacto === '' || email === '' ) {
		alert( 'Por favor ingrese todos los campos obligatorios.' );
	} else {
		jQuery.ajax({
			type: 'POST',
			url: 'build/app.php',
			data: dataString,
			cache: false,
			success: function ( html ) {
				//alert( html );
				jQuery( '#myModal' ).modal( 'show' );
			}
		});
		jQuery( '#form' ).each(function () {
			this.reset();
		});
	}
	return false;
}