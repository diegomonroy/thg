<%@ Control language="vb" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
    <%@ Register TagPrefix="dnn" TagName="LANGUAGE" Src="~/Admin/Skins/Language.ascx" %>
        <%@ Register TagPrefix="dnn" TagName="LOGO" Src="~/Admin/Skins/Logo.ascx" %>
            <%@ Register TagPrefix="dnn" TagName="SEARCH" Src="~/Admin/Skins/Search.ascx" %>
                <%@ Register TagPrefix="dnn" TagName="BREADCRUMB" Src="~/Admin/Skins/BreadCrumb.ascx" %>
                    <%@ Register TagPrefix="dnn" TagName="USER" Src="~/Admin/Skins/User.ascx" %>
                        <%@ Register TagPrefix="dnn" TagName="LOGIN" Src="~/Admin/Skins/Login.ascx" %>
                            <%@ Register TagPrefix="dnn" TagName="PRIVACY" Src="~/Admin/Skins/Privacy.ascx" %>
                                <%@ Register TagPrefix="dnn" TagName="TERMS" Src="~/Admin/Skins/Terms.ascx" %>
                                    <%@ Register TagPrefix="dnn" TagName="COPYRIGHT" Src="~/Admin/Skins/Copyright.ascx" %>
                                        <%@ Register TagPrefix="dnn" TagName="LINKTOMOBILE" Src="~/Admin/Skins/LinkToMobileSite.ascx" %>
                                            <%@ Register TagPrefix="dnn" TagName="JQUERY" Src="~/Admin/Skins/jQuery.ascx" %>
                                                <%@ Register TagPrefix="dnn" TagName="META" Src="~/Admin/Skins/Meta.ascx" %>
                                                    <%@ Register TagPrefix="dnn" TagName="MENU" src="~/DesktopModules/DDRMenu/Menu.ascx" %>
                                                        <%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
                                                            <%@ Register TagPrefix="dnn" TagName="MYBOOKINS" Src="~/DesktopModules/Netactica/MyBookings/MyBookings.ascx" %>
                                                                <dnn:META ID="mobileScale" runat="server" Name="viewport" Content="width=device-width,initial-scale=1" />

                                                                <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
                                                                <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
                                                                <link href="/Portals/167/Skins/epale/contenido.css" rel="stylesheet" type="text/css" />
																<link href="/Portals/167/Skins/epale/build/app.css" rel="stylesheet" type="text/css">

                                                                <dnn:JQUERY ID="dnnjQuery" runat="server" jQueryHoverIntent="true" />
                                                                <dnn:DnnJsInclude ID="bootstrapJS" runat="server" FilePath="bootstrap/js/bootstrap.min.js" PathNameAlias="SkinPath" />
                                                                <dnn:DnnJsInclude ID="customJS" runat="server" FilePath="js/scripts.js" PathNameAlias="SkinPath" />

                                                                <script src="/Portals/167/Skins/epale/js/fixEspejo.js" charset="utf-8"></script>

                                                                <script runat="server">
                                                                    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
                                                                    Me.Page.ClientScript.RegisterClientScriptInclude("netactica.scripts.js", "/js/Netactica/netactica.scripts.js")
                                                                    End Sub
                                                                </script>

                                                                <script type="text/javascript">
                                                                    PortalMirrors("167", "epale");
                                                                </script>

                                                                <script type="text/javascript" src="/portals/167/skins/epale/js/scroll_page.js"></script>
                                                                <script>
                                                                    function visible(element) {
                                                                        var elemento = document.getElementById(element);
                                                                        elemento.style.display = 'block';
                                                                    }
                                                                </script>

                                                                <script>
                                                                    function invisible(element) {
                                                                        var elemento = document.getElementById(element);
                                                                        elemento.style.display = 'none';
                                                                    }
                                                                </script>
                                                                <div id="siteWrapper">

                                                                    <div class="zona_redes_sociales hidden-phone">
                                                                        <!-- Zona redes sociales -->
                                                                        <div class="item_redes_sociales">
                                                                            <a href=“#” target="_blank" title="Facebook"><img src="/Portals/167/ico_facebook.png" width="30" height="30" alt="Facebook" /></a>
                                                                        </div>
                                                                        <div class="item_redes_sociales">
                                                                            <a href=“#” target="_blank" title="Twitter"><img src="/Portals/167/ico_twitter.png" width="30" height="30" alt="Twitter" /></a>
                                                                        </div>
                                                                        <div class="item_redes_sociales" style="display:none;">
                                                                            <a href="#"><img src="/Portals/167/ico_instagram.png" width="30" height="30" alt="Instagram" /></a>
                                                                        </div>
                                                                    </div>
                                                                    <!-- Fin Zona redes sociales -->

                                                                    <div class="zona_redes_sociales_alumni">
                                                                        <!-- zona_redes_sociales_alumni -->
                                                                        <div class="item_redes_sociales">
                                                                            <a href="#" target="_blank" title="Facebook"><img src="/Portals/167/ico_facebook.png" width="30" height="30" alt="Facebook" /></a>
                                                                        </div>
                                                                        <div class="item_redes_sociales">
                                                                            <a href="#" target="_blank" title="Twitter"><img src="/Portals/167/ico_twitter.png" width="30" height="30" alt="Twitter" /></a>
                                                                        </div>
                                                                        <div class="item_redes_sociales" style="display:none;">
                                                                            <a href="#"><img src="/Portals/167/ico_instagram.png" width="30" height="30" alt="Instagram" /></a>
                                                                        </div>
                                                                    </div>
                                                                    <!-- Fin zona_redes_sociales_alumni -->

                                                                    <div id="header_top">
                                                                        <div class="container">
                                                                            <div class="row-fluid">
                                                                                <div class="span11 offset1">
                                                                                    <div class="row-fluid"> </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <!--/userControls-->
                                                                    <div id="header_content">

                                                                        <div class="container">
                                                                            <div class="row-fluid fondo_header">
                                                                                <div class="span12">
                                                                                    <div class="row-fluid">
                                                                                        <!-- open row-fluid -->

                                                                                        <div id="top-menu" class="span8 hidden-phone">
                                                                                            <div class="top-menu-ini">
                                                                                                <div class="zona_registrese">
                                                                                                    <dnn:USER runat="server" id="dnnUSER" CssClass="user" />
                                                                                                </div>
                                                                                                <div class="zona_acceso_usuarios">
                                                                                                    <dnn:LOGIN runat="server" id="dnnLOGIN" CssClass="login" />
                                                                                                </div>
                                                                                                <div class="zona_mis_reservas">
                                                                                                    <dnn:MYBOOKINS runat="server" id="dnnMYBOOKINS" CssClass="misreservas" />
                                                                                                </div>

                                                                                            </div>
                                                                                            <div id="top-social" class="span4 offset2">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div id="header_logo" class="span4_logo">
                                                                                            <span class="brand">
                                                                                              <dnn:LOGO runat="server" id="dnnLOGO" />
                                                                                          </span>
                                                                                        </div>
                                                                                        <div id="logo" class="span4_logo"><!--<img src="/portals/167/reedmac-logo.jpg">--></div>
                                                                                        <div class="zona_redes_sociales_mobile hidden-tablet hidden-desktop">
                                                                                            <!-- Zona redes sociales -->
                                                                                            <div class="item_redes_sociales">
                                                                                                <a href=“#” target="_blank" title="Facebook"><img src="/Portals/167/ico_facebook.png" width="30" height="30" alt="Facebook" /></a>
                                                                                            </div>
                                                                                            <div class="item_redes_sociales">
                                                                                                <a href=“#” target="_blank" title="Twitter"><img src="/Portals/167/ico_twitter.png" width="30" height="30" alt="Twitter" /></a>
                                                                                            </div>
                                                                                            <div class="item_redes_sociales" style="display:none;">
                                                                                                <a href="#"><img src="/Portals/167/ico_instagram.png" width="30" height="30" alt="Instagram" /></a>
                                                                                            </div>
                                                                                        </div>
                                                                                        <!-- close menu_principal_pag -->

                                                                                        <div class="clear"></div>
                                                                                    </div>
                                                                                    <!-- close row-fluid -->

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="clear"></div>
                                                                    </div>
																	<!-- Begin Menu -->
																	<div class="menu_principal_pag">
																		<div class="container">
                                                                                            <!--open menu_principal_pag -->


                                                                                            <div class="navbar">

                                                                                                <div class="navbar-inner"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><i class="fa fa-bars"></i></a>
                                                                                                    <div id="top-menu" class="span8 hidden-tablet hidden-desktop">
                                                                                                        <div class="top-menu-ini">
                                                                                                            <div class="zona_registrese_mobile">
                                                                                                                <dnn:USER runat="server" id="dnnUSERMobile" CssClass="user" /> |
                                                                                                            </div>
                                                                                                            <div class="zona_acceso_usuarios_mobile">
                                                                                                                <dnn:LOGIN runat="server" id="dnnLOGINMobile" CssClass="login" /> |
                                                                                                            </div>
                                                                                                            <div class="zona_mis_reservas_mobile">
                                                                                                                <dnn:MYBOOKINS runat="server" id="dnnMYBOOKINSMobile" CssClass="misreservas" />
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="nav-collapse collapse">
                                                                                                        <dnn:MENU ID="bootstrapNav" MenuStyle="bootstrapNav" runat="server"></dnn:MENU>
                                                                                                    </div>
                                                                                                    <!-- END nav-collapse -->
                                                                                                </div>
                                                                                                <!-- END navbar-inner -->
                                                                                            </div>
                                                                                            <!-- END navbar -->

																		</div>
																	</div>
																	<!-- End Menu -->
                                                                    <div id="siteHeadouter">
                                                                        <div id="siteHeadinner" class="container">
                                                                            <!--
      <div class="row-fluid">
        <div class="span12 offset">
          <div class="txt_copy">Dale <span>click</span> a la felicidad</div>
        </div>
      </div> -->
                                                                        </div>
                                                                        <!--/siteHeadinner-->
                                                                    </div>
                                                                    <!--/siteHeadouter-->
                                                                    <div id="widgetWrapper">
                                                                        <div class="container">
                                                                            <div class="row-fluid">
                                                                                <div id="ContentPane2" class="contentPane" runat="server"></div>
                                                                            </div>
                                                                            <div class="row-fluid">
                                                                                <div id="ContentPane" class="span10 offset1 contentPane" runat="server"></div>
                                                                                 
                                                                            </div>
                                                                            <div class="row-fluid">
                                                                                <div id="leftPane" class="span4 leftPane spacingTop" runat="server"></div>
                                                                                <div id="sidebarPane" class="span8 sidebarPane spacingTop" runat="server"></div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="wrapper_destacados">
                                                                            <div class="container">
                                                                                <div class="row-fluid">
                                                                                    <div id="contentPaneLower" class="span12 contentPane spacingTop" runat="server"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="container">
                                                                            <div class="row-fluid">
                                                                                <div id="contentPaneLower2" class="span12 contentPane spacingTop" runat="server"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--/contentWrapper -->
                                                                    <div id="footer">
                                                                        <div class="row-fluid footer_top">

                                                                            <div class="container">
                                                                                <!-- Abro container -->
                                                                                <div class="span12">
                                                                                    <div class="row-fluid">
                                                                                        <!-- Abro row-fluid -->

                                                                                        <div class="span2 menu_footer hidden-tablet hidden-phone">
                                                                                            <ul>
                                                                                                <li><a href="/Acerca-de-Nosotros">Acerca de Nosotros</a></li>
                                                                                                <li><a href="/Pol%C3%ADtica-de-Privacidad">Política de Privacidad</a></li>
                                                                                                <li><a href="/T%C3%A9rminos-y-Condiciones">Términos y Condiciones</a></li>
                                                                                                <li><a href="/Cont%C3%A1ctenos">Contáctenos</a></li>
                                                                                            </ul>
                                                                                        </div>

                                                                                        <!-- Menu footer para alumni -->
                                                                                        <div class="span2 menu_footer_alumni hidden-tablet hidden-phone">
                                                                                            <ul>
                                                                                                <li><a href="/Acerca-de-Nosotros">Acerca de Alumni</a></li>
                                                                                                <li><a href="/Pol%C3%ADtica-de-Privacidad">Política de Privacidad</a></li>
                                                                                                <li><a href="/T%C3%A9rminos-y-Condiciones">Términos y Condiciones</a></li>
                                                                                                <li><a href="/Cont%C3%A1ctenos">Contáctenos</a></li>
                                                                                            </ul>
                                                                                        </div>
                                                                                        <!-- fin menu footer para alumni-->

                                                                                        <div class="span2 menu_footer hidden-tablet hidden-phone" style="visibility:hidden;">
                                                                                            <ul>
                                                                                                <li><a href="#">Ir al Home</a></li>
                                                                                                <li><a href="#">Más Buscados</a></li>
                                                                                                <li><a href="#">Paquetes Destacados</a></li>
                                                                                                <li><a href="#">Nuestros Recomendados</a></li>
                                                                                            </ul>
                                                                                        </div>

                                                                                        <div class="span6 offset2 contacto_footer">
                                                                                            Book Travel Center<br> Calle 111 No.14-16 Piso 3, Oficina 303<br> Tel: +57 3153314397 | email: contacto@booktravelcenter.com <br>
                                                                                        </div>

                                                                                        <div class="span6 offset3 contacto_footer_alumni">
                                                                                            ALUMNI TRAVEL &amp; MEETINGS<br> Carrera 64 No. 94a - 51 Bogotá, Colombia<br> Tel: 2710800 | email: info@alumni.com.co<br>
                                                                                        </div>

                                                                                    </div>
                                                                                    <!-- Cierro row-fluid -->
                                                                                    <div id="footerA" class="span3 footerPane" runat="server"></div>
                                                                                    <div id="footerB" class="span2 footerPane" runat="server"></div>
                                                                                    <div id="footerC" class="span3 footerPane" runat="server"></div>
                                                                                    <div id="footerD" class="span3 footerPane" runat="server"></div>
                                                                                </div>
                                                                            </div>
                                                                            <!-- Cierro container -->

                                                                        </div>
                                                                        <div class="copyrights_footer">

                                                                            <div class="container">
                                                                                <!-- Abro container -->
                                                                                <div class="row-fluid">

                                                                                    <div class="span4"><img src="/Portals/167/Skins/epale/Images/logo_iata.jpg" width="129" height="42" alt="IATA" />&nbsp;&nbsp;&nbsp;<img src="/Portals/167/Skins/epale/Images/Logo_placetopay.png"
                                                                                            width="97" height="42" alt="Place to Pay" /> &nbsp;&nbsp;&nbsp;
                                                                                        <img src="/Portals/167/Skins/epale/Images/logo_ssl.gif" width="97" height="42" alt="Comodo" /> &nbsp;&nbsp;&nbsp;
                                                                                        <img src="/Portals/167/Skins/epale/Images/Logo_PSE.png" width="97" height="42" alt="PSE" />
                                                                                    </div>

                                                                                    <div class="span4 offset4">
                                                                                        <div id="logoNetactica">
                                                                                            <a class="netactica" href="http://www.netactica.com/" title="Powered by Netactica" target="_blank"></a>
                                                                                        </div>
                                                                                        <div class="logo_epale_footer"><img src="/Portals/167/Logo.png" width="369" height="122" /></div>
                                                                                        <div class="logo_alumni_footer"><img src="/Portals/167/Logo-alumni.png" width="369" height="122" /></div>
                                                                                    </div>
                                                                                    <div class="span5 footer_derechos">Todos los derechos reservados a BOOK TRAVEL CENTER © 2017 </div>
                                                                                    <div class="span5 footer_derechos_alumni">Todos los derechos reservados © 2017</div>
                                                                                    <div class="span7 footer_derechos zona_menu_footer_legales" style="text-align:right; font-size:12px;"><a href="http://www.sic.gov.co/drupal/turismo" target="_blank">Superintendencia de Industria y Comercio</a>&nbsp; | &nbsp;<a href="http://www.aerocivil.gov.co/Paginas/default.aspx"
                                                                                            target="_blank">Aeronáutica Civil</a>&nbsp; | &nbsp;<a href="http://www.aerocivil.gov.co/AAeronautica/Rrglamentacion/Resoluciones/Documents/RESL.%20N%C2%B0%2002591%20%20Junio%20%20%20%20%2006%20de%202013.pdf"
                                                                                            target="_blank">Derechos y Deberes del Pasajero</a>&nbsp; | &nbsp;<a href="http://www.alcaldiabogota.gov.co/sisjur/normas/Norma1.jsp?i=44306" target="_blank">Estatuto del Consumidor</a></div>

                                                                                    <!--/copyright-->
                                                                                </div>
                                                                            </div>
                                                                            <!-- Abro cierro -->



                                                                        </div>
                                                                    </div>
                                                                    <!--/footer-->

                                                                </div>
                                                                <!--/siteWrapper-->
                                                                <script>
                                                                    (function() {
                                                                        $(document).ready(function(e) {
                                                                            $(".DnnModule-DNN_AmazingSlider").addClass("hidden-phone hidden-tablet");
                                                                        });
                                                                    })();
                                                                </script>

                                                                <script>
                                                                    if ($("#netactica_booking_form").length > 0 && $("#special_options_toggle_netactica_air").length > 0) {
                                                                        $('#arrow_aditional_details_netactica_air').removeClass('ui-icon-triangle-1-s').addClass('ui-icon-triangle-1-n');
                                                                        $('#special_options_netactica_air').show("fast");
                                                                    }
$( 'ul.nav li:first-child a' ).addClass( 'inicio' );
$( 'a.inicio' ).attr( 'href', 'http://www.booktravelcenter.com' );
																</script>